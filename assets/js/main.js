var cadastros = []

// Chama a Função LerCadastros para popular as tabelas ao carregar o site
$(function () {
    cadastros = JSON.parse(localStorage.getItem("__cadastros__")) ?? []

    if (cadastros) {
        LerCadastros()
    }
})

function is_cpf(c) {

    if ((c = c.replace(/[^\d]/g, "")).length != 11)
        return false

    if (c == "00000000000")
        return false;

    if (c.length !== 11 || !Array.from(c).filter(e => e !== c[0]).length) {
        return false
    }


    var r;
    var s = 0;

    for (i = 1; i <= 9; i++)
        s = s + parseInt(c[i - 1]) * (11 - i);

    r = (s * 10) % 11;

    if ((r == 10) || (r == 11))
        r = 0;

    if (r != parseInt(c[9]))
        return false;

    s = 0;

    for (i = 1; i <= 10; i++)
        s = s + parseInt(c[i - 1]) * (12 - i);

    r = (s * 10) % 11;

    if ((r == 10) || (r == 11))
        r = 0;

    if (r != parseInt(c[10]))
        return false;

    return true;
}

// Funções de Validação e mascara de CPF 
function fMasc(objeto, mascara) {
    obj = objeto
    masc = mascara
    setTimeout("fMascEx()", 1)
}

function fMascEx() {
    obj.value = masc(obj.value)
}

function mCPF(cpf) {
    cpf = cpf.replace(/\D/g, "")
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2")
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2")
    cpf = cpf.replace(/(\d{3})(\d{1,2})$/, "$1-$2")
    return cpf
}

cpfCheck = function (el) {
    if (is_cpf(el.value)) {
        document.getElementById("inputCpf").setCustomValidity("");
    } else {
        document.getElementById("inputCpf").setCustomValidity("Insira um CPF válido!");
    }
}

// Funções Para o popular o select de Estado e Cidade
const selectEstados = document.querySelector('#selectEstado');
const selectCidades = document.querySelector('#selectCidade');

function SeletorEstados() {
    fetch('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
        .then(res => res.json())
        .then(estados => {

            estados.map(estados => {
                const option = document.createElement('option');

                option.setAttribute('id', estados.id);
                option.setAttribute('value', estados.nome + "-" + estados.sigla + "-" + estados.id);
                option.textContent = estados.nome + "-" + estados.sigla;

                selectEstados.appendChild(option);

            });
        })
}

function SeletorCidade(editEstado, editCidade) {
    if (editEstado == undefined) {
        selectEstados.addEventListener('change', () => {

            let nodesSelectCidades = selectCidades.childNodes;

            [...nodesSelectCidades].map(node => node.remove());

            let estado = selectEstados.options[selectEstados.selectedIndex].id;

            fetch(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${estado}/municipios`)
                .then(res => res.json())
                .then(cidades => {

                    selectCidades.removeAttribute('disabled');

                    cidades.map(cidade => {

                        const option = document.createElement('option');

                        option.setAttribute('value', cidade.nome);
                        option.textContent = cidade.nome;

                        selectCidades.appendChild(option);
                    });
                })
        });
    }
    else {
        let nodesSelectCidades = selectCidades.childNodes;

        [...nodesSelectCidades].map(node => node.remove());

        fetch(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${editEstado}/municipios`)
            .then(res => res.json())
            .then(cidades => {

                selectCidades.removeAttribute('disabled');

                cidades.map(cidade => {

                    const option = document.createElement('option');

                    option.setAttribute('value', cidade.nome);

                    if (cidade.nome == editCidade) {
                        option.setAttribute('selected', '');
                    }

                    option.textContent = cidade.nome;

                    selectCidades.appendChild(option);
                });
            })
    }
}

SeletorEstados();
SeletorCidade();

// Função para calcular a idade o usuario
function CalcularIdade(DtNascimento) {
    var dataAtual = new Date();
    var anoAtual = dataAtual.getFullYear();
    var anoNascParts = DtNascimento.split('/');
    var diaNasc = anoNascParts[0];
    var mesNasc = anoNascParts[1];
    var anoNasc = anoNascParts[2];
    var idade = anoAtual - anoNasc;
    var mesAtual = dataAtual.getMonth() + 1;
    if (mesAtual < mesNasc) {
        idade--;
    } else {
        if (mesAtual == mesNasc) {
            if (new Date().getDate() < diaNasc) {
                //Se a data atual for menor que o dia de nascimento ele ainda nao fez aniversario
                idade--;
            }
        }
    }
    return idade;
}

// Pega a data atual e limita o input com o atributo "max"
var hoje = new Date();
var dia = hoje.getDate();
var mes = hoje.getMonth() + 1;
var ano = hoje.getFullYear();
if (dia < 10) {
    dia = '0' + dia
}
if (mes < 10) {
    mes = '0' + mes
}

hoje = ano + '-' + mes + '-' + dia;
document.getElementById("inputDtNascimento").setAttribute("max", hoje);

// Chama o modal para confirmara ação apagar usuario
function ModalApagarCadastro(id) {
    var idObj = id;
    console.log(id)
    $('#modalApagar').modal('show')
    document.getElementById("btnModalApagar").setAttribute("value", id);
    console.log(id)
}

//*Delete* apos confirmado apaga o usuario 
function ApagarCadastros(id) {

    for (let i = 0; i < cadastros.length; i++) {
        if (cadastros[i].ID == id) {
            cadastros.splice(i, 1)
        }
    }
    $('#modalApagar').modal('hide')
    LerCadastros()

}

//*Update* Edita os cadastros de usuario 
function EditarCadastros(id) {
    cadastros.forEach(function (cad) {
        if (cad.ID == id) {
            var Estado = cad.Estado.split('-');
            var EstadoID = Estado[2];
            var cidadeValue = cad.Cidade;
            $('#cadID').val(cad.ID)
            $('#inputNome').val(cad.Nome)
            $('#inputCpf').val(cad.Cpf)
            $('#inputDtNascimento').val(cad.DtNascimento.substr(6, 4) + "-" + cad.DtNascimento.substr(3, 2) + "-" + cad.DtNascimento.substr(0, 2))
            $('#selectEstado').val(cad.Estado)
            SeletorCidade(EstadoID, cidadeValue)
        }
    })
}

//*Read* popula as tabelas quando site for carregado ou algum cadastro for adicionado/alterado
function LerCadastros() {
    localStorage.setItem("__cadastros__", JSON.stringify(cadastros))

    $('#tblCadUser tbody').html("")

    if (Array.isArray(cadastros)) {

        cadastros.forEach(function (cad) {
            var Estado = cad.Estado.split('-');
            var EstadoSigla = Estado[1];
            var EstadoNome = Estado[0] + "-" + Estado[1]

            $('#tblCadUser tbody').append(`
            <tr>
                <td>${cad.Nome}</td>
                <td class="thMobileSm">${cad.Cpf}</td>
                <td class="thDesktop">${cad.DtNascimento}</td>
                <td class="thDesktop">${cad.Idade}</td>
                <td class="thDesktop">${EstadoSigla}</td>
                <td class="thDesktop">${cad.Cidade}</td>
                <td class="thMobile" style="text-align:center"><button type="button" class="tblBtn" data-bs-toggle="modal" data-bs-target="#modalDetail-${cad.ID}"><i class="fas fa-eye"></i></button></td>
                <td style="text-align:center"><button type="button" class="tblBtn" onclick="javascript:EditarCadastros(${cad.ID})"><i class="fas fa-pencil-alt"></i></button></td>
                <td style="text-align:center"><button type="button" class="tblBtn" onclick="javascript:ModalApagarCadastro(${cad.ID})"><i class="fas fa-trash-alt"></i></button></td>
            </tr>
            `)
            $('#tblCadUser').append(`
            <div class="modal fade" id="modalDetail-${cad.ID}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">${cad.Nome}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p><strong>CPF: </strong><br/>${cad.Cpf}</p>
                        <p><strong>DATA DE NASCIMENTO : </strong><br/>${cad.DtNascimento}</p>
                        <p><strong>IDADE: </strong><br/>${cad.Idade}</p>
                        <p><strong>ESTADO: </strong><br/>${EstadoNome}</p>
                        <p><strong>CIDADE: </strong><br/>${cad.Cidade}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                    </div>
                    </div>
                </div>
            </div> 
            `)
        })
    }
}

//*Create* função para criar ou editar cadastros
$('#cadUser').submit(function (e) {
    e.preventDefault();

    let _id = $('#cadID').val()
    let Nome = $('#inputNome').val()
    let Cpf = $('#inputCpf').val()
    let DtNascimento = new Date($('#inputDtNascimento').val()).toLocaleDateString("pt-br", { timeZone: "UTC" })
    let Idade = CalcularIdade(DtNascimento)
    let Estado = document.getElementById('selectEstado').options[selectEstados.selectedIndex].value;
    let Cidade = $('#selectCidade').val()

    if (!_id || _id == "0") {

        let registro = {}

        registro.Nome = Nome
        registro.Cpf = Cpf
        registro.DtNascimento = DtNascimento
        registro.Idade = Idade
        registro.Estado = Estado
        registro.Cidade = Cidade
        registro.ID = cadastros.length + 1

        cadastros.push(registro)
        $('#modalCadastrado').modal('show')

    }
    else {
        cadastros.forEach(function (cad) {
            if (cad.ID == _id) {
                cad.Nome = Nome
                cad.Cpf = Cpf
                cad.DtNascimento = DtNascimento
                cad.Idade = Idade
                cad.Estado = Estado
                cad.Cidade = Cidade
            }
        })
        $('#modalEditado').modal('show')
    }


    $('#cadID').val("0")
    $('#inputNome').val("")
    $('#inputCpf').val("")
    $('#inputDtNascimento').val("")
    $('#selectEstado').val("")
    $('#selectCidade').val("")
    $('#selectCidade').attr('disabled', true);

    LerCadastros()
});