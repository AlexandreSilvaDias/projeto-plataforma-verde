<?php 
    require('components/header.php');
?>
<main>
    <div class="cadContainer">
        <div class="topTitle">
            <div class="content">
                <img src="assets/images/iconUser.svg" alt="Icone Cadastro Usuário">
                <h1 class="mainTitle">Cadastro Usuário</h1>
                <div class="info">
                    <img class="iconInfo" src="assets/images/iconInfo.svg" alt="Icone de Informação">
                    <div class="infoDetails">
                        <i class="fas fa-times-circle"></i>
                        <p>
                            Cadastre os usuários que terão acesso ao sistema
                        </p>
                    </div>
                </div>
            </div>
            <p>* campos obrigatórios</p>
        </div>
        <section class="formContainer">
            <form id="cadUser" class="formGrid" action="" method="post">
                <input type="hidden" id="cadID">
                <fieldset class="fieldset name">
                    <label for="nome">Nome Completo*</label>
                    <input id="inputNome" class="input" type="text" required="required" />
                </fieldset>
                <fieldset class="fieldset cpf">
                    <label for="cpf">CPF*</label>
                    <input id="inputCpf" class="input" type="text" placeholder="000.000.000-00" onblur="cpfCheck(this)" maxlength="14" onkeydown="javascript: fMasc( this, mCPF );" required="required">
                </fieldset>
                <fieldset class="fieldset data">
                    <label for="nascimento">Data de Nascimento*</label>
                    <input id="inputDtNascimento" class="input date" type="date" autocomplete="off" placeholder="dd/mm/aaaa" min="1900-01-01" required="required" />
                </fieldset>
                <fieldset class="fieldset estado">
                    <label for="estado">Estado*</label>
                    <select id="selectEstado" class="input" required="required" >
                        <option value="">Selecione</option>
                    </select>
                </fieldset>
                <fieldset class="fieldset cidade">
                    <label for="cidade">Cidade*</label>
                    <select id="selectCidade" class="input" disabled required="required">
                        <option value="">Selecione</option>
                    </select>
                </fieldset>
                <fieldset class="fieldset enviar">
                    <button id="btnIncluir" type="submit" class="cadBtn btn-green"><i class="fas fa-arrow-down"></i>INCLUIR</button>
                </fieldset>
            </form>
            <table id="tblCadUser" class="table tableCadUser table-responsive">
                <thead>
                    <tr>
                        <th class="thW250" scope="col">NOME COMPLETO</th>
                        <th class="thW140 thMobileSm" scope="col">CPF</th>
                        <th class="thW180 thDesktop" scope="col">DATA DE NASCIMENTO</th>
                        <th class="thW100 thDesktop" scope="col">IDADE</th>
                        <th class="thW100 thDesktop" scope="col">ESTADO</th>
                        <th class="thW204 thDesktop" scope="col">CIDADE</th>
                        <th class="thW100 thMobile" scope="col">VER MAIS</th>
                        <th class="thW100" scope="col">EDITAR</th>
                        <th class="thW100" scope="col">EXCLUIR</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <div class="modal fade" id="modalCadastrado" tabindex="-1" aria-labelledby="usuário cadastrado" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">USUÁRIO CADASTRADO</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fechar"></button>
                    </div>
                    <div class="modal-body modal-confirm">
                        <i class="far fa-check-circle"></i>
                        <P>O usúario foi cadastrado com sucesso!</P>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                    </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalEditado" tabindex="-1" aria-labelledby="cadastro alterado" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">CADASTRADO ALTERADO</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fechar"></button>
                    </div>
                    <div class="modal-body modal-confirm">
                        <i class="far fa-check-circle"></i>
                        <P>O cadastrado do usúario foi alterado com sucesso!</P>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                    </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalApagar" tabindex="-1" aria-labelledby="deletar usuário" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">DESEJA APAGAR ESSE USUÁRIO?</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fechar"></button>
                    </div>
                    <div class="modal-body modal-confirm ">
                    <i class="far fa-times-circle"></i>
                        <P>Deseja Deletar este usuário? <br/>Essa ação não poderá ser desfeita!</P>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="btnModalApagar" onclick="javascript:ApagarCadastros(value)">Confirmar</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                    </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php 
    require('components/footer.php');
?>